import {
	Page,
	Alert,
	NavController
} from 'ionic-angular';

import {
	Push
} from 'ionic-native';

import {
	Http
} from '@angular/http';

const URL = 'https://plan.luca-steeb.com';
//const URL = 'http://192.168.0.101:5000';

function alert(_, title, message) {
	let alert = Alert.create({
		title: title,
		subTitle: message,
		buttons: ['Ok']
	});

	_.nav.present(alert);
}

@Page({
	templateUrl: 'build/pages/page1/page1.html'
})
export class Page1 {
	constructor(http, nav) {
		var _ = this;

		this.http = http;
		this.nav = nav;
		this.courses = [];

		let push = Push.init({
			android: {
				senderID: '755412640729'
			}
		});

		_.user = {} //class: 'E2', courses: [ "W1", "S2" ]

		if (!push || push.error) return;

		try {
			push.on('registration', (data) => {
				console.log('registration successful: ' + data.registrationId);
				console.log('saving regId in this');
				_.registrationId = data.registrationId;

				this.http.get(
					URL + '/api/getUser' +
					'?token=' + _.registrationId
				).subscribe(result => {

					result._body = JSON.parse(result._body);

					console.log('result:', result._body);

					if (result._body.success) {
						_.user = result._body.user;
					}
				}, error => {
					alert(this, 'Fehler', 'Sorry, da ist mir wohl ein Fehler unterlaufen.. (constructor:http-err1:page1.js)');
				});
			});
		} catch (e) {
			alert(this, 'Fehler', 'Sorry, da ist mir wohl ein Fehler unterlaufen.. (constructor:catch:page1.js)');
		}
	}

	static get parameters() {
		return [
			[Http],
			[NavController]
		];
	}

	ready() {
		var _ = this;
		console.log(this.courses);

		if (!this.class || ~this.class.indexOf('--')) {
			alert(this, 'Fehler', 'Du hast vergessen deine Klasse auszuwählen. :) (ready:if:page1.js)');
			return;
		}

		console.log('using this.registrationId', typeof _.registrationId);

		this.http.get(
			URL + '/api/createUser' +
			'?token=' + _.registrationId +
			'&class=' + this.class +
			this.courses.map((item, i) => {
				return '&courses[' + i + ']=' + item;
			}).join('')
		).subscribe(result => {

			result._body = JSON.parse(result._body);

			if (result._body.success) {
				alert(this, 'Erfolg', 'Perfekt. Jetzt bekommst du automatisch eine Benachrichtigung, falls etwas ausfallen oder verschoben sollte.');
			}
		}, error => {
			alert(this, 'Fehler', 'Sorry, da ist mir wohl ein Fehler unterlaufen.. (ready:http-err2:page1.js)');
		});
	}
}
