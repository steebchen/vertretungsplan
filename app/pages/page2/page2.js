import {
	Page,
	Alert,
	NavController
} from 'ionic-angular';

import {
	Push
} from 'ionic-native';

import {
	Http
} from '@angular/http';

const URL = 'https://plan.luca-steeb.com';
// const URL = 'http://192.168.0.101:5000';

function go(_) {
	_.http.get(URL + '/api/getIssues?token=' + _.registrationId).subscribe(result => {
		result._body = JSON.parse(result._body);

		if (result._body) {
			_.results = result._body;
		}
	}, error => {
		alert(this, 'Fehler', 'Sorry, da ist mir wohl ein Fehler unterlaufen.. (constructor:http:page2.js)');
	});
}

function alert(_, title, message) {
	let alert = Alert.create({
		title: title,
		subTitle: message,
		buttons: ['OK']
	});

	_.nav.present(alert);
}

@Page({
	templateUrl: 'build/pages/page2/page2.html'
})
export class Page2 {
	static get parameters() {
		return [
			[Http],
			[NavController]
		];
	}

	constructor(http, nav) {
		let _ = this;

		_.http = http;
		_.nav = nav;

		_.results = []

		let push = Push.init({
			android: {
				senderID: '755412640729'
			}
		});

		if (!push || push.error) return;

		try {
			push.on('registration', (data) => {
				_.registrationId = data.registrationId;
				go(_)
			});
		} catch (e) {
			alert(this, 'Fehler', 'Sorry, da ist mir wohl ein Fehler unterlaufen.. (constructor:reg:page2.js)');
			go(_)
		}
	}

	refresh() {
		go(this)
	}
}
